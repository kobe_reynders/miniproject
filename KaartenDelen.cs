﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koetje_Melken
{
   public class KaartenDelen
    {
        private int count;

        private Cards[] player = new Cards[26];
        private Cards[] computer = new Cards[26];
        private DeckOfCards cardsDeck = new DeckOfCards();

        public KaartenDelen()
        {
            DeelKaarten();
        }

        public void DeelKaarten()
        {
            cardsDeck.maakDeck();
            count = 0;

            for (int i = 1; i <= 52; i += 2)
            {
                player[count] = cardsDeck.GetDeck[i];
                count++;
            }

            count = 0;

            for (int i = 2; i <= 52; i += 2)
            {
                computer[count] = cardsDeck.GetDeck[i];
                count++;
            }
        }

        public Cards[] GetPlayerCards { get { return player; } }
        public Cards[] GetComputerCards { get { return computer; } }

        public String cardName(Cards card)
        {
            return Convert.ToString(card.cardValue).ToLower() + " of " + Convert.ToString(card.cardSuit).ToLower();
        }

        public String cardValue(Cards card)
        {
            return Convert.ToString(card.cardValue);
        }
    }
}
