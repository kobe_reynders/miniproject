﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Koetje_Melken
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private KaartenDelen deck = new KaartenDelen();

        private int playcount = 0;
        private int count = 25;
        private int computerScore = 0;
        private int playerScore = 0;
        private int numberOfDraws = 0;

        DispatcherTimer computerDrawTimer = new DispatcherTimer();
        DispatcherTimer clearTimer = new DispatcherTimer();

        BitmapImage bi = new BitmapImage();
        Image backOfCard_computer = new Image();
        Image backOfCard_player = new Image();
        Image playCardImage_Computer = new Image();
        Image playCardImage_Player = new Image();



        public MainWindow()
        {
            InitializeComponent();
            drawButton.IsEnabled = false;
            computerDrawTimer.Interval = TimeSpan.FromMilliseconds(2000);
            clearTimer.Interval = TimeSpan.FromMilliseconds(2000);
            computerDrawTimer.Tick += ComputerDrawTimer_Tick;
            clearTimer.Tick += ClearTimer_Tick;


        }
        

        private void shuffleButton_Click(object sender, RoutedEventArgs e)
        {
            if (playcount == 0)
            {
                bi.BeginInit();
                bi.UriSource = new Uri(@"C:\Users\Kobe\ownCloud\NET Essentials\Koetje_Melken\Afbeeldingen\cardback.png", UriKind.RelativeOrAbsolute);
                bi.EndInit();
            }

            backOfCard_computer.Source = bi;
            backOfCard_computer.Width = 65;
            backOfCard_computer.Height = 87;
            backCard_Computer.Children.Add(backOfCard_computer);

            backOfCard_player.Source = bi;
            backOfCard_player.Width = 65;
            backOfCard_player.Height = 87;
            backCard_Player.Children.Add(backOfCard_player);

            deck.DeelKaarten();
            notificationBlock.Text = "De kaarten zijn geschud, u kan nu beginnen met spelen.";
            computerLabel.Text = "";
            playerLabel.Text = "";
            numberOfDrawsLabel_Computer.Text = "";
            numberOfDrawsLabel_Player.Text = "";
            computerDrawTimer.Start();

            playerScore = 0;
            computerScore = 0;
            numberOfDraws = 0;
            count = 25;
            playcount++;
            shuffleButton.IsEnabled = false;
        }


        private void drawButton_Click(object sender, RoutedEventArgs e)
        {
            computerDrawTimer.Stop();
            if (count >= 0)
            {

                ShowPlayerCard(deck.GetPlayerCards[count]);

                numberOfDrawsLabel_Player.Text = "#" + numberOfDraws;
                playerLabel.Text = deck.cardName(deck.GetPlayerCards[count]);
                evaluate_Cards();
                count--;
                drawButton.IsEnabled = false;
            }

            clearTimer.Start();
            
        }


        private void backCard_Player_MouseUp(object sender, MouseButtonEventArgs e)
        {
            drawButton_Click(sender, e);
        }


        private void evaluate_Cards()
        {
            if (CardValue(deck.GetComputerCards[count]) > CardValue(deck.GetPlayerCards[count]))
            {
                notificationBlock.Text = "Computer wins!";
                computerScore++;
            }
            else if ((CardValue(deck.GetComputerCards[count]) < CardValue(deck.GetPlayerCards[count])))
            {
                notificationBlock.Text = "You win!";
                playerScore++;
            }
            else
            {
                notificationBlock.Text = "Draw.";
                playerScore++;
                computerScore++;
            }

            if (numberOfDraws == 26)
            {
                clearTimer.Stop();
                computerLabel.Text = "NO MORE CARDS TO DEAL.";
                playerLabel.Text = "NO MORE CARDS TO DEAL.";
                numberOfDrawsLabel_Computer.Text = "Click shuffle cards to continue";
                numberOfDrawsLabel_Player.Text = "Click shuffle cards to continue";
                backCard_Computer.Children.Clear();
                playCard_Computer.Children.Clear();
                backCard_Player.Children.Clear();
                playCard_Player.Children.Clear();
                drawButton.IsEnabled = false;
                shuffleButton.IsEnabled = true;
                
                
                if (computerScore > playerScore)
                {
                    notificationBlock.Text = "Sorry, you lost. The computer won " + computerScore + " times, you won " + playerScore + " times.";
                }
                else if (computerScore < playerScore)
                {
                    notificationBlock.Text = "Congratulations!! You are the winner!! The computer won " + computerScore + " times, you won " + playerScore + " times.";
                }
                else
                {
                    notificationBlock.Text = "No winner, you have the same result as the computer: you won " + playerScore + " times.";
                }
            }
        }


        private int CardValue(Cards card)
        {
            int cardvalue = 0;
            switch (card.cardValue)
            {
                case VALUE.Deuce:
                    cardvalue = 1;
                    break;
                case VALUE.Three:
                    cardvalue = 2;
                    break;
                case VALUE.Four:
                    cardvalue = 3;
                    break;
                case VALUE.Five:
                    cardvalue = 4;
                    break;
                case VALUE.Six:
                    cardvalue = 5;
                    break;
                case VALUE.Seven:
                    cardvalue = 6;
                    break;
                case VALUE.Eight:
                    cardvalue = 7;
                    break;
                case VALUE.Nine:
                    cardvalue = 8;
                    break;
                case VALUE.Ten:
                    cardvalue = 9;
                    break;
                case VALUE.Jack:
                    cardvalue = 10;
                    break;
                case VALUE.Queen:
                    cardvalue = 11;
                    break;
                case VALUE.King:
                    cardvalue = 12;
                    break;
                case VALUE.Ace:
                    cardvalue = 13;
                    break;
                default:
                    cardvalue = 0;
                    break;
            }
            return cardvalue;
        }


        private void ComputerDrawTimer_Tick(object sender, EventArgs e)
        {
            numberOfDraws++;
            if (count >= 0)
            {
                ShowComputerCard(deck.GetComputerCards[count]);

                numberOfDrawsLabel_Computer.Text = "#" + numberOfDraws;
                computerLabel.Text = deck.cardName(deck.GetComputerCards[count]);
                drawButton.IsEnabled = true;
                computerDrawTimer.Stop();
            }
        }


        private void ClearTimer_Tick(object sender, EventArgs e)
        {
            if (count >= 0)
            {
                computerLabel.Text = "";
                playerLabel.Text = "";
                notificationBlock.Text = "";
                playCard_Computer.Children.Clear();
                playCard_Player.Children.Clear();
                computerDrawTimer.Start();
                clearTimer.Stop();
            }

        }


        private void ShowComputerCard(Cards card)
        {
            BitmapImage computerImage = new BitmapImage();
            computerImage.BeginInit();

            computerImage.UriSource = new Uri(@"C:\Users\Kobe\ownCloud\NET Essentials\Koetje_Melken\Afbeeldingen\" + Convert.ToString(card.cardValue) +
               Convert.ToString(card.cardSuit) + ".png", UriKind.RelativeOrAbsolute);


            computerImage.EndInit();
            playCardImage_Computer.Source = computerImage;
            playCardImage_Computer.Width = 65;
            playCardImage_Computer.Height = 87;
            playCard_Computer.Children.Add(playCardImage_Computer);
        }


        private void ShowPlayerCard(Cards card)
        {
            BitmapImage playerImage = new BitmapImage();
            playerImage.BeginInit();

            playerImage.UriSource = new Uri(@"C:\Users\Kobe\ownCloud\NET Essentials\Koetje_Melken\Afbeeldingen\" + Convert.ToString(card.cardValue) + 
                Convert.ToString(card.cardSuit) + ".png", UriKind.RelativeOrAbsolute);


            playerImage.EndInit();
            playCardImage_Player.Source = playerImage;
            playCardImage_Player.Width = 65;
            playCardImage_Player.Height = 87;
            playCard_Player.Children.Add(playCardImage_Player);
        }

        
    }
}
