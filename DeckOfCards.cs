﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koetje_Melken
{
    public class DeckOfCards : Cards
    {

        const int AantalKaarten = 53; //aantal kaarten in 1 deck
        private Cards[] deck; //array met alle kaarten van het deck


        public DeckOfCards()
        {
            deck = new Cards[AantalKaarten];
        }


        public Cards[] GetDeck { get { return deck; } } //methode voor het huidige deck op te vragen.

        public void maakDeck()
        {
            int i = 0;
            foreach (SUIT s in Enum.GetValues(typeof(SUIT)))
            {
                foreach (VALUE v in Enum.GetValues(typeof(VALUE)))
                {
                    deck[i] = new Cards { cardSuit = s, cardValue = v };
                    i++;
                }
            }
            shuffle();
        }

        public void shuffle()
        {
            Random rand = new Random();
            Cards hulp;

            for (int aantalKeerShuffle = 0; aantalKeerShuffle < 100; aantalKeerShuffle++)
            {
                for (int i = 0; i < AantalKaarten; i++)
                {
                    int kaartNummer = rand.Next(13);
                    hulp = deck[i];
                    deck[i] = deck[kaartNummer];
                    deck[kaartNummer] = hulp;
                }
            }
        }

    }
}
